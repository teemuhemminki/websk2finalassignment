#Final Assignment#
This project was part of a web application development course in JAMK. It works as a frontend for java backend, that can be found inside java_ee_course repository (https://bitbucket.org/teemuhemminki/java_ee_course/src/master/Teht%C3%A4v%C3%A4setti%204/springkeycloakdemo/). It also uses keycloak for user authentication.  
Project is based on this tutorial: https://www.callicoder.com/spring-boot-mongodb-angular-js-rest-api-tutorial/  and it also uses keycloak-angular package (https://www.npmjs.com/package/keycloak-angular)  
Project is commented in finnish.
  
Copyright 2018 Teemu Hemminki  
  
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  
  
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  
  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.