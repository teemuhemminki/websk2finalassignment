import { KeycloakService } from 'keycloak-angular';

/*
Tällä alustetaan keycloak autentikointi.
Alustuksessa annetaan osoite, realm ja client keycloak puolella.
initOptionsissa sanotaan, että käyttäjän on kirjauduttava sisään.
*/
export function initializer(keycloak: KeycloakService): () => Promise<any> {
 return (): Promise<any> => {
   return new Promise(async (resolve, reject) => {
     try {
       await keycloak.init({
           config: {
               url: 'http://localhost:8180/auth',
               realm: 'SpringKeycloakDemo',
               clientId: 'login-app',
           },
           initOptions: {
             onLoad: 'login-required',
             checkLoginIframe: false
           },
           enableBearerInterceptor: true
       });
       resolve();
     } catch (error) {
         reject(error);
     }
   });
 };
}