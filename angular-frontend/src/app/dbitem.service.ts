import { Injectable } from '@angular/core';
import { DbItem } from './DbItem';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';

/*
DbItemService ottaa yhteyttä backendiin ja suorittaa CRUD
toimenpiteitä sinne.
*/

@Injectable()
export class DbItemService {
    private baseUrl = 'http://localhost:8080';

    constructor(private http: Http) { }

    getDbItems(): Promise<DbItem[]> {
        return this.http.get(this.baseUrl + '/api/dbitems')
            .toPromise()
            .then(response => response.json() as DbItem[])
            .catch(this.handleError);
    }

    createDbItem(dbItemData: DbItem): Promise<DbItem> {
        return this.http.post(this.baseUrl + '/api/dbitems/', dbItemData)
            .toPromise().then(response => response.json() as DbItem)
            .catch(this.handleError);
    }

    updateDbItem(dbItemData: DbItem): Promise<DbItem> {
        return this.http.put(this.baseUrl + '/api/dbitems/' + dbItemData.id, dbItemData)
            .toPromise()
            .then(response => response.json() as DbItem)
            .catch(this.handleError);
    }

    deleteDbItem(id: string): Promise<any> {
        return this.http.delete(this.baseUrl + '/api/dbitems/' + id)
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('Some error occured', error);
        return Promise.reject(error.message || error);
    }
}
