//Malli tietokanta esineelle
export class DbItem {
    id: string;
    title: string;
    completed: boolean;
    createdAt: Date;
    owner: number;
}