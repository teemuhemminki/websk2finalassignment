import { Component, OnInit } from '@angular/core';
import { DbItem } from './dbitem';
import { DbItemService } from './dbitem.service';
import { NgForm } from '@angular/forms';

/*
DbItemListComponent käyttää DbItemServiceä, hakeakseen ja päivittääkseen
elementtejä näytettävässä listassa.
*/

@Component({
    selector: 'dbitem-list', //css määritelyä varten
    templateUrl: './dbitem-list.component.html' //url templaatti
})

export class DbItemListComponent implements OnInit {
    dbItems: DbItem[];
    newDbItem: DbItem = new DbItem();
    editing: boolean = false;
    editingDbItem: DbItem = new DbItem();

    constructor(
        private dbItemService: DbItemService,
    ){}

    ngOnInit(): void {
        this.getDbItems();
    }

    getDbItems(): void {
        this.dbItemService.getDbItems()
        .then(dbItems => this.dbItems = dbItems);
    }

    createDbItem(dbItemForm: NgForm): void {
        this.dbItemService.createDbItem(this.newDbItem)
        .then(createDbItem => {
            dbItemForm.reset();
            this.newDbItem = new DbItem();
            this.dbItems.unshift(createDbItem)
        });
    }

    deleteDbItem(id: string): void {
        this.dbItemService.deleteDbItem(id)
        .then(()=>{
            this.dbItems = this.dbItems.filter(dbItem => dbItem.id != id);
        })
    }

    updateDbItem(dbItemData: DbItem): void {
        console.log(dbItemData);
        this.dbItemService.updateDbItem(dbItemData)
        .then(updatedDbItem => {
            let existingDbItem = this.dbItems.find(dbItem => dbItem.id === updatedDbItem.id);
            Object.assign(existingDbItem, updatedDbItem);
            this.clearEditing();
        })
    }

    toggleCompleted(dbItemData: DbItem): void {
        dbItemData.completed = !dbItemData.completed;
        this.dbItemService.updateDbItem(dbItemData)
        .then(updatedDbItem => {
            let existingDbItem = this.dbItems.find(dbItem => dbItem.id === updatedDbItem.id);
        });
    }

    editDbItem(dbItemData: DbItem): void {
        this.editing = true;
        Object.assign(this.editingDbItem, dbItemData);
    }

    clearEditing(): void {
        this.editingDbItem = new DbItem();
        this.editing = false;
    }
}