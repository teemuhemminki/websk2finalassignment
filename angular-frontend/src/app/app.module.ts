import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DbItemService } from './dbitem.service';

import { DbItemListComponent } from './dbitem-list.component';

//Haetaan Angular-Keycloakin vaatimat importit
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';
import { initializer } from './app-init';

@NgModule({
  declarations: [
    AppComponent,
    DbItemListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    KeycloakAngularModule //Tuodaan keycloak angular importti käyttöön
  ],
  providers: [DbItemService, {
    //Alla alustetaan ja otetaan keycloak palvelu käyttöön
    provide: APP_INITIALIZER, useFactory: initializer,
    multi: true, deps: [KeycloakService]
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
